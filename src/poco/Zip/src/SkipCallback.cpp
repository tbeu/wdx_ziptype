//
// SkipCallback.cpp
//
// Library: Zip
// Package: Zip
// Module:  SkipCallback
//
// Copyright (c) 2007, Applied Informatics Software Engineering GmbH.
// and Contributors.
//
// SPDX-License-Identifier:	BSL-1.0
//


#include "Poco/Zip/SkipCallback.h"
#include "Poco/Zip/ZipLocalFileHeader.h"
#include "Poco/Zip/ZipUtil.h"
#include "Poco/Exception.h"


namespace Poco {
namespace Zip {


SkipCallback::SkipCallback()
{
}


SkipCallback::~SkipCallback()
{
}


bool SkipCallback::handleZipEntry(std::istream& zipStream, const ZipLocalFileHeader& hdr)
{
	return true;
}


} } // namespace Poco::Zip
