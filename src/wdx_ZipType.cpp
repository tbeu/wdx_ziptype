// wdx_ZipType.cpp : Definiert die exportierten Funktionen für die DLL-Anwendung.
//

#include "stdafx.h"
#include "cunicode.h"
#include "contplug.h"  // WDX
#include <gsl/gsl_util>
#include <Poco/Exception.h>
#include <Poco/FileStream.h>
#include <Poco/Path.h>
#include <Poco/UnicodeConverter.h>
#include <Poco/Zip/ParseCallback.h>
#include <Poco/Zip/ZipArchive.h>
#include <Poco/Zip/ZipLocalFileHeader.h>
#include <Poco/Zip/ZipUtil.h>
#include <fstream>
#include <optional>
#include <set>
#include <sstream>

using CompressionLevel = Poco::Zip::ZipCommon::CompressionLevel;
using CompressionMethod = Poco::Zip::ZipCommon::CompressionMethod;
static constexpr const int HEADER_SIZE{ Poco::Zip::ZipCommon::HEADER_SIZE };
static constexpr const char detectstring[]{
    "([0]=\"P\" & [1]=\"K\" & [2]=3 & [3]=4) | ([0]=\"P\" & [1]=\"K\" & [2]=7 & [3]=8)"
};
static constexpr const int fieldcount{ 14 };
static const char *fieldnames[fieldcount]{ "ZipType",
                                           "Zip64",
                                           "Files",
                                           "Directories",
                                           "Total files",
                                           "Encrypted",
                                           "Ratio",
                                           "Comment",
                                           "Compressed File Size",
                                           "Uncompressed File Size",
                                           "Compression Level(s)",
                                           "Compression Method(s)",
                                           "Pack version",
                                           "Unpack version" };
static constexpr const int fieldtypes[fieldcount]{ ft_boolean,    ft_boolean,    ft_numeric_32,       ft_numeric_32,
                                                   ft_numeric_32, ft_boolean,    ft_numeric_floating, ft_stringw,
                                                   ft_numeric_64, ft_numeric_64, ft_string,           ft_string,
                                                   ft_string,     ft_string };
static const char *fieldunits_and_multiplechoicestrings[fieldcount]{
    "", "", "", "", "", "", "", "", "bytes|kbytes|Mbytes|Gbytes", "bytes|kbytes|Mbytes|Gbytes", "", "", "", ""
};

enum class EFields
{
    ZipType = 0,
    Zip64,
    Files,
    Directories,
    TotalFiles,
    Encryped,
    Ratio,
    Comment,
    CompressedFileSize,
    UncompressedFileSize,
    CompressionLevel,
    CompressionMethod,
    PackVersion,
    UnpackVersion,
};

struct CriticalSection : CRITICAL_SECTION
{
    CriticalSection()
    {
        InitializeCriticalSection(this);
    }

    ~CriticalSection()
    {
        DeleteCriticalSection(this);
    }
};
static CriticalSection cs;
struct CriticalSectionHandler
{
    CriticalSectionHandler()
    {
        EnterCriticalSection(&cs);
    }

    ~CriticalSectionHandler()
    {
        LeaveCriticalSection(&cs);
    }
};

struct ZipInfo
{
    wchar_t fileName[wdirtypemax]{ L"" };
    Poco::Int32 numberOfFiles{ 0 };
    Poco::Int32 numberOfDirs{ 0 };
    Poco::Int64 compressedSize{ 0 };
    Poco::Int64 uncompressedSize{ 0 };
    double ratio{ 0. };
    std::string comment{ "" };
    std::set<CompressionLevel> compressionLevel{};
    std::set<CompressionMethod> compressionMethod{};
    bool isZipType{ false };
    bool isEncrypted{ false };
    int packVerMaj{ 0 };
    int packVerMin{ 0 };
    int unpackVerMaj{ 0 };
    int unpackVerMin{ 0 };
    bool needsZip64{ false };
};

static HINSTANCE hinst{ nullptr };
static ZipInfo fileCache{};

BOOL APIENTRY DllMain(HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpvReserved)
{
    switch (fdwReason) {
        case DLL_PROCESS_ATTACH:
            hinst = hinstDLL;
            ::DisableThreadLibraryCalls(hinst);
            break;

        case DLL_PROCESS_DETACH:
            hinst = nullptr;
            break;
    }
    return TRUE;
}

class ZipCallback : public Poco::Zip::ParseCallback
{
public:
    ZipCallback(ZipInfo &zipInfo) : zipInfo{ zipInfo }
    {
    }
    virtual ~ZipCallback() = default;

    bool handleZipEntry(std::istream &, const Poco::Zip::ZipLocalFileHeader &header)
    {
        if (header.isDirectory()) {
            zipInfo.numberOfDirs++;
        } else if (header.isFile()) {
            zipInfo.numberOfFiles++;
            zipInfo.compressionLevel.emplace(CompressionLevel(header.getCompressionLevel()));
            zipInfo.compressionMethod.emplace(CompressionMethod(header.getCompressionMethod()));
        }
        zipInfo.compressedSize += header.getCompressedSize();
        zipInfo.uncompressedSize += header.getUncompressedSize();
        zipInfo.isEncrypted = zipInfo.isEncrypted || header.isEncrypted();
        if (header.needsZip64()) {
            zipInfo.needsZip64 = true;
        }
        return true;
    }

private:
    ZipInfo &zipInfo;
};

static bool CheckFile(const wchar_t *FileName, ZipInfo &zipInfo)
{
    CriticalSectionHandler csh;
    if (0 == std::wcscmp(fileCache.fileName, FileName)) {
        zipInfo = fileCache;
        return fileCache.isZipType;
    }

    wcslcpy(zipInfo.fileName, FileName, wdirtypemax);

    std::string utf8fileName;
    Poco::UnicodeConverter::convert(FileName, utf8fileName);
    Poco::Path fileName{ utf8fileName };

    auto is = Poco::FileInputStream();
    try {
        is.open(fileName.toString(), std::ifstream::binary);
    } catch (Poco::Exception &) {
        fileCache.isZipType = false;
        return false;
    }
    if (!is.good()) {
        fileCache.isZipType = false;
        return false;
    }
    const auto closeStream = gsl::finally([&is] { is.close(); });
    {
        char buf[HEADER_SIZE];
        is.read(buf, HEADER_SIZE);
        if (0 != std::memcmp(buf, "PK\x03\x04", HEADER_SIZE) && 0 != std::memcmp(buf, "PK\x07\x08", HEADER_SIZE)) {
            fileCache.isZipType = false;
            return false;
        }
    }

    is.clear();
    is.seekg(0, std::ios::beg);

    auto cb = ZipCallback(zipInfo);
    const Poco::Zip::ZipArchive zai(is, cb);
    zipInfo.comment = zai.getZipComment();
    if (zipInfo.compressionMethod.empty() && zipInfo.numberOfFiles > 0) {
        zipInfo.compressionMethod.emplace(CompressionMethod::CM_STORE);
    }
    if (const auto it = zai.headerBegin(); it != zai.headerEnd()) {
        const auto &firstHeader = *it->get();
        firstHeader.getRequiredVersion(zipInfo.unpackVerMaj, zipInfo.unpackVerMin);
    }
    if (const auto it = zai.fileInfoBegin(); it != zai.fileInfoEnd()) {
        const auto &firstInfo = *it->get();
        firstInfo.getVersionMadeBy(zipInfo.packVerMaj, zipInfo.packVerMin);
        firstInfo.getRequiredVersion(zipInfo.unpackVerMaj, zipInfo.unpackVerMin);
    }
    if (zipInfo.uncompressedSize > 0) {
        zipInfo.ratio = 100. * zipInfo.compressedSize / zipInfo.uncompressedSize;
    } else {
        zipInfo.ratio = 0.;
    }
    zipInfo.isZipType = true;
    fileCache = zipInfo;
    return fileCache.isZipType;
}

static char *strlcpy(char *str1, const char *str2, int maxSize)
{
    if (int(std::strlen(str2)) >= maxSize - 1) {
        std::strncpy(str1, str2, maxSize - 1);
        str1[maxSize - 1] = '\0';
    } else {
        std::strcpy(str1, str2);
    }
    return str1;
}

inline static Poco::Int64 ConvertSize(Poco::Int64 fileSize, int UnitIndex)
{
    switch (UnitIndex) {
        case 1:
            fileSize /= 1024;
            break;
        case 2:
            fileSize /= (1024 * 1024);
            break;
        case 3:
            fileSize /= (1024 * 1024 * 1024);
            break;
        default:
            break;
    }
    return fileSize;
}

inline static const char *ConvertLevel(Poco::Zip::ZipCommon::CompressionLevel level)
{
    switch (level) {
        case Poco::Zip::ZipCommon::CompressionLevel::CL_FAST:
            return "Fast";
        case Poco::Zip::ZipCommon::CompressionLevel::CL_MAXIMUM:
            return "Maximum";
        case Poco::Zip::ZipCommon::CompressionLevel::CL_NORMAL:
            return "Normal";
        case Poco::Zip::ZipCommon::CompressionLevel::CL_SUPERFAST:
            return "Superfast";
        default:
            return "";
    }
}

inline static const char *ConvertMethod(Poco::Zip::ZipCommon::CompressionMethod method)
{
    static char buf[10];
    switch (method) {
        case Poco::Zip::ZipCommon::CompressionMethod::CM_STORE:
            return "Stored";
        case Poco::Zip::ZipCommon::CompressionMethod::CM_SHRUNK:
            return "Shrunk";
        case Poco::Zip::ZipCommon::CompressionMethod::CM_FACTOR1:
            return "Factor1";
        case Poco::Zip::ZipCommon::CompressionMethod::CM_FACTOR2:
            return "Factor2";
        case Poco::Zip::ZipCommon::CompressionMethod::CM_FACTOR3:
            return "Factor3";
        case Poco::Zip::ZipCommon::CompressionMethod::CM_FACTOR4:
            return "Factor4";
        case Poco::Zip::ZipCommon::CompressionMethod::CM_IMPLODE:
            return "Imploded";
        case Poco::Zip::ZipCommon::CompressionMethod::CM_TOKENIZE:
            return "Tokenized";
        case Poco::Zip::ZipCommon::CompressionMethod::CM_DEFLATE:
            return "Deflated";
        case Poco::Zip::ZipCommon::CompressionMethod::CM_ENHANCEDDEFLATE:
            return "Deflated64";
        case Poco::Zip::ZipCommon::CompressionMethod::CM_BZIP2:
            return "BZIP2";
        case Poco::Zip::ZipCommon::CompressionMethod::CM_LZMA:
            return "LZMA";
        case Poco::Zip::ZipCommon::CompressionMethod::CM_IBM_CMPSC:
            return "IBM z/OS CMPSC";
        case Poco::Zip::ZipCommon::CompressionMethod::CM_IBM_TERSE:
            return "IBM TERSE (new)";
        case Poco::Zip::ZipCommon::CompressionMethod::CM_IBM_LZ77:
            return "IBM LZ77";
        case Poco::Zip::ZipCommon::CompressionMethod::CM_DEPRECATED20:
            return "LPAQ8";
        case Poco::Zip::ZipCommon::CompressionMethod::CM_ZSTD:
            return "Zstandard (zstd)";
        case Poco::Zip::ZipCommon::CompressionMethod::CM_MP3:
            return "MP3";
        case Poco::Zip::ZipCommon::CompressionMethod::CM_XZ:
            return "XZ";
        case Poco::Zip::ZipCommon::CompressionMethod::CM_JPEG:
            return "JPEG";
        case Poco::Zip::ZipCommon::CompressionMethod::CM_WAVPACK:
            return "WavPack";
        case Poco::Zip::ZipCommon::CompressionMethod::CM_PPMD:
            return "PPMd version I, Rev 1";
        case Poco::Zip::ZipCommon::CompressionMethod::CM_AES:
            return "AE-x";
        default:
            snprintf(buf, 10, "%u", static_cast<unsigned>(method));
            return buf;
    }
}

int __stdcall ContentGetDetectString(char *DetectString, int maxlen)
{
    strlcpy(DetectString, detectstring, maxlen);
    return 0;
}

int __stdcall ContentGetSupportedField(int FieldIndex, char *FieldName, char *Units, int maxlen)
{
    if (FieldIndex < 0 || FieldIndex >= fieldcount)
        return ft_nomorefields;
    strlcpy(FieldName, fieldnames[FieldIndex], maxlen);
    strlcpy(Units, fieldunits_and_multiplechoicestrings[FieldIndex], maxlen);
    return fieldtypes[FieldIndex];
}

int __stdcall ContentGetValue(const char *FileName, int FieldIndex, int UnitIndex, void *FieldValue, int maxSize,
                              int flags)
{
    std::vector<wchar_t> FileNameW;
    FileNameW.reserve(std::strlen(FileName) + 1);
    const int rc{ ContentGetValueW(awlcopy(FileNameW.data(), FileName, int(FileNameW.capacity())), FieldIndex,
                                   UnitIndex, FieldValue, maxSize, flags) };

    if (ft_numeric_floating == rc && maxSize > sizeof(double)) {
        wchar_t *p = _wcsdup((const wchar_t *)(static_cast<char *>(FieldValue) + sizeof(double)));
        walcopy(static_cast<char *>(FieldValue) + sizeof(double), p, maxSize - sizeof(double));
        free(p);
    }
    return rc;
}

int __stdcall ContentGetValueW(const wchar_t *FileName, int FieldIndex, int UnitIndex, void *FieldValue, int maxSize,
                               int flags)
{
    ZipInfo zipInfo{};
    if (!CheckFile(FileName, zipInfo)) {
        if (EFields::ZipType == EFields(FieldIndex)) {
            WIN32_FIND_DATAW findFileData;
            if (auto handle = FindFirstFileT(zipInfo.fileName, &findFileData); INVALID_HANDLE_VALUE != handle) {
                const auto closeHandle = gsl::finally([&handle] { FindClose(handle); });
                if (findFileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) {
                    return ft_fieldempty;
                }
                *static_cast<Poco::Int32 *>(FieldValue) = 0;
                return fieldtypes[FieldIndex];
            }
            return ft_fileerror;
        } else {
            return ft_fieldempty;
        }
    }

    switch (EFields(FieldIndex)) {
        case EFields::ZipType:
            *static_cast<Poco::Int32 *>(FieldValue) = 1;
            break;
        case EFields::Zip64:
            *static_cast<Poco::Int32 *>(FieldValue) = zipInfo.needsZip64;
            break;
        case EFields::Files:
            *static_cast<Poco::Int32 *>(FieldValue) = zipInfo.numberOfFiles;
            break;
        case EFields::Directories:
            *static_cast<Poco::Int32 *>(FieldValue) = zipInfo.numberOfDirs;
            break;
        case EFields::TotalFiles:
            *static_cast<Poco::Int32 *>(FieldValue) = zipInfo.numberOfFiles + zipInfo.numberOfDirs;
            break;
        case EFields::Encryped:
            *static_cast<Poco::Int32 *>(FieldValue) = zipInfo.isEncrypted;
            break;
        case EFields::Ratio:
            *static_cast<double *>(FieldValue) = zipInfo.ratio;
            break;
        case EFields::Comment:
            if (zipInfo.comment.empty()) {
                return ft_fieldempty;
            }
            {
                auto *buf = [&zipInfo]() -> wchar_t * {
                    for (const UINT cp : { CP_UTF8, CP_ACP, CP_OEMCP, CP_MACCP, 1250, 1251, 1252, 1253, 1254, 1255,
                                           1256, 1257, 1258, 936, 950, 932, 949, 874 }) {
                        if (wchar_t *buf = cp2u(cp, zipInfo.comment.c_str()); buf)
                            return buf;
                    }
                    return nullptr;
                }();
                if (buf) {
                    wcslcpy(static_cast<wchar_t *>(FieldValue), buf, maxSize / 2);
                    delete[] buf;
                } else {
                    return ft_notsupported;
                }
            }
            break;
        case EFields::CompressedFileSize:
            *static_cast<Poco::Int64 *>(FieldValue) = ConvertSize(zipInfo.compressedSize, UnitIndex);
            break;
        case EFields::UncompressedFileSize:
            *static_cast<Poco::Int64 *>(FieldValue) = ConvertSize(zipInfo.uncompressedSize, UnitIndex);
            break;
        case EFields::CompressionLevel:
            if (zipInfo.compressionLevel.empty()) {
                return ft_fieldempty;
            }
            {
                bool isFirst = true;
                for (const auto cl : zipInfo.compressionLevel) {
                    if (isFirst) {
                        strlcpy(static_cast<char *>(FieldValue), ConvertLevel(cl), maxSize);
                        isFirst = false;
                    } else {
                        strncat(static_cast<char *>(FieldValue), ", ", maxSize);
                        strncat(static_cast<char *>(FieldValue), ConvertLevel(cl), maxSize);
                    }
                }
            }
            break;
        case EFields::CompressionMethod:
            if (zipInfo.compressionMethod.empty()) {
                return ft_fieldempty;
            }
            {
                bool isFirst = true;
                for (const auto cm : zipInfo.compressionMethod) {
                    if (isFirst) {
                        strlcpy(static_cast<char *>(FieldValue), ConvertMethod(cm), maxSize);
                        isFirst = false;
                    } else {
                        strncat(static_cast<char *>(FieldValue), ", ", maxSize);
                        strncat(static_cast<char *>(FieldValue), ConvertMethod(cm), maxSize);
                    }
                }
            }
            break;
        case EFields::PackVersion:
            if (0 == zipInfo.packVerMaj && 0 == zipInfo.packVerMin) {
                return ft_fieldempty;
            }
            snprintf(static_cast<char *>(FieldValue), maxSize, "%d.%d", zipInfo.packVerMaj, zipInfo.packVerMin);
            break;
        case EFields::UnpackVersion:
            if (0 == zipInfo.unpackVerMaj && 0 == zipInfo.unpackVerMin) {
                return ft_fieldempty;
            }
            snprintf(static_cast<char *>(FieldValue), maxSize, "%d.%d", zipInfo.unpackVerMaj, zipInfo.unpackVerMin);
            break;
        default:
            return ft_nosuchfield;
    }
    return fieldtypes[FieldIndex];  // very important!
}

void __stdcall ContentPluginUnloading(void)
{
    CriticalSectionHandler csh;
    fileCache.fileName[0] = L'\0';
}
