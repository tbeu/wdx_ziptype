ZipType plugin 2.1.1.1 for Total Commander
==========================================

 * License:
-----------

Copyright (C) 2021-2024 by Thomas Beutlich

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.


 * Installation:
----------------

Open the plugin archive using Total Commander and installation will start.


 * Description:
---------------

ZipType is a content plugin for Total Commander to get various information about
ZIP archives.

The supported fields are:
- Zip type flag
- Zip64 flag
- Number of files
- Number of directories
- Total number of files (Number of files + Number of directories)
- Encrypted flag
- Ratio %
- Comment
- Compressed file size
- Uncompressed file size
- Compression levels
- Compression methods
- Pack version
- Unpack version

 * ChangeLog:
-------------

 o Version 2.1.1.1 (06.09.2024)
   - fixed resource leak
 o Version 2.1.1.0 (05.09.2024)
   - updated zlib to v1.3.1
   - fixed field name Encrypted
   - updated value of field ZipType if not a zip archive
   - improved run-time performance
   - reduced memory foot-print
 o Version 2.1.0.0 (21.08.2024)
   - added fields for Zip64, Pack version and Unpack version
   - fixed comment encoding
   - fixed exception on invalid file stream
   - fixed exception if ZIP archive contains duplicated entries
   - fixed exception on data descriptor detection
   - fixed exception on compression methods > 11
   - fixed compression levels to consider all files
   - fixed compression methods to consider all files
 o Version 2.0.2.0 (31.07.2024)
   - updated Poco to v1.13.3
 o Version 2.0.1.0 (06.07.2022)
   - fixed Unicode path issue
   - updated Poco to v1.11.3
 o Version 2.0.0.0 (11.02.2021)
   - reimplemented


 * References:
--------------

 o Guidelines Support Library
   - https://github.com/microsoft/GSL
 o POCO C++ Libraries: Foundation, Zip
   - https://github.com/pocoproject/poco
 o Content-Plugin Writer's Guide by Christian Ghisler
   - https://ghisler.github.io/WDX-SDK


 * Trademark and Copyright Statements:
--------------------------------------

 o Total Commander is Copyright © 1993-2024 by Christian Ghisler, Ghisler Software GmbH.
   - http://www.ghisler.com


 * Feedback:
------------

If you have problems, questions, suggestions please contact Thomas Beutlich.
 o Email: support@tbeu.de
 o URL: http://tbeu.totalcmd.net